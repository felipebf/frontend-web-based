-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Dez-2017 às 16:33
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scriptbd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `IdPessoa` int(11) NOT NULL,
  `Nome` varchar(80) DEFAULT NULL,
  `Datanasc` text,
  `datab` date DEFAULT NULL,
  `Cpf` varchar(11) DEFAULT NULL,
  `Logradouro` varchar(50) NOT NULL,
  `Numero` smallint(6) NOT NULL,
  `Bairro` varchar(50) NOT NULL,
  `Cidade` varchar(50) NOT NULL,
  `CEP` int(11) NOT NULL,
  `Complemento` varchar(30) DEFAULT NULL,
  `CodigoIbge` tinyint(4) NOT NULL,
  `IdSexo` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`IdPessoa`, `Nome`, `Datanasc`, `datab`, `Cpf`, `Logradouro`, `Numero`, `Bairro`, `Cidade`, `CEP`, `Complemento`, `CodigoIbge`, `IdSexo`) VALUES
(1, 'Felipe Borges Ferreira', '06/11/1991', '1991-11-04', '12345678910', 'RUA MINAS GERAIS', 1200, 'Cristo Redentor', 'PATOS DE MINAS', 38700262, 'Perto do barr', 31, 1),
(3, 'Lorenzo Cabril', '08/04/1964', NULL, '33074074574', 'Rua Santo AntÃ´nio', 565, 'Dez', 'Salvador', 41999000, '', 29, 1),
(4, 'Mariana Santos Alves', '08/03/1973', NULL, '71491380047', 'Rua Principal sul', 801, 'Siqueira Lima', 'SÃ£o Paulo', 8000000, 'Casa de esquina', 35, 2),
(5, 'Maria Fernanda Oliveira', '05/12/1984', '1984-12-05', '96456460301', 'Avenida Seis', 1166, 'Siqueira Lima', 'MacapÃ¡', 68914000, '', 16, 2),
(6, 'Diogo Carvalho Galisteu', '09/09/1979', NULL, '69920148709', 'Av. Dom Pedro II', 1981, 'Hozias', 'Boa Vista', 69339000, '', 14, 1),
(7, 'TomÃ¡s Frias Galileu', '22/08/1955', NULL, '19317936628', 'Rua Nove', 43, 'Huzek', 'Natal', 59000000, 'Fundos', 24, 1),
(8, 'Cristiano Oliveira SidÃ£o', '28/02/1999', NULL, '50409673145', 'Rua G', 1976, 'Conjunto C', 'SÃ£o Paulo', 8000000, '', 35, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sexo`
--

CREATE TABLE `sexo` (
  `IdSexo` tinyint(2) NOT NULL,
  `Sigl` varchar(3) NOT NULL,
  `Descricao` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sexo`
--

INSERT INTO `sexo` (`IdSexo`, `Sigl`, `Descricao`) VALUES
(1, 'M', 'Masculino'),
(2, 'F', 'Feminino');

-- --------------------------------------------------------

--
-- Estrutura da tabela `unidadefederativa`
--

CREATE TABLE `unidadefederativa` (
  `CodigoIbge` tinyint(4) NOT NULL,
  `Sigla` varchar(3) NOT NULL,
  `NomeEstado` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `unidadefederativa`
--

INSERT INTO `unidadefederativa` (`CodigoIbge`, `Sigla`, `NomeEstado`) VALUES
(12, 'AC', 'Acre'),
(27, 'AL', 'Alagoas'),
(13, 'AM', 'Amazonas'),
(16, 'AP', 'Amapá'),
(29, 'BA', 'Bahia'),
(23, 'CE', 'Ceará'),
(53, 'DF', 'Distrito Federal'),
(32, 'ES', 'Espírito Santo'),
(52, 'GO', 'Goiás'),
(21, 'MA', 'Maranhão'),
(31, 'MG', 'Minas Gerais'),
(50, 'MS', 'Mato Grosso do Sul'),
(51, 'MT', 'Mato Grosso'),
(15, 'PA', 'Pará'),
(25, 'PB', 'Paraíba'),
(26, 'PE', 'Pernambuco'),
(22, 'PI', 'Piauí'),
(41, 'PR', 'Paraná'),
(33, 'RJ', 'Rio de Janeiro'),
(24, 'RN', 'Rio Grande do Norte'),
(11, 'RO', 'Rondônia'),
(14, 'RR', 'Roraima'),
(43, 'RS', 'Rio Grande do Sul'),
(42, 'SC', 'Santa Catarina'),
(28, 'SE', 'Sergipe'),
(35, 'SP', 'São Paulo'),
(17, 'TO', 'Tocantins');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`IdPessoa`),
  ADD KEY `unidadefederativa_endereco_fk` (`CodigoIbge`),
  ADD KEY `IdSexo` (`IdSexo`);

--
-- Indexes for table `sexo`
--
ALTER TABLE `sexo`
  ADD PRIMARY KEY (`IdSexo`);

--
-- Indexes for table `unidadefederativa`
--
ALTER TABLE `unidadefederativa`
  ADD PRIMARY KEY (`CodigoIbge`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `IdPessoa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
