<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/CadastroUsuarioCrud/config/BancoDados.php";

class CadastrarPessoaModel{

	private $bd;

	function __construct(){
		$this->bd = BancoDados::obterConexao();
	}

	public function inserir($nome, $cpf, $sexo, $datanasc, $logadouro, $numero, $bairro, $cidade, $uf, $cep, $complemento){
		$insercao = $this->bd->prepare("INSERT INTO pessoa(Nome, Cpf, IdSexo, Datanasc, Logradouro, Numero, Bairro, Cidade, CodigoIbge, CEP, Complemento)
			values(:nome, :cpf, :sexo, :datanasc, :end, :nume, :bai, :cid, :uf, :cep, :compl)");
		$insercao->bindParam(":nome", $nome);
		$insercao->bindParam(":datanasc", $datanasc);
		$insercao->bindParam(":cpf", $cpf);
		$insercao->bindParam(":sexo", $sexo);
		$insercao->bindParam(":end", $logadouro);
		$insercao->bindParam(":nume", $numero);
		$insercao->bindParam(":bai", $bairro);
		$insercao->bindParam(":cid", $cidade);
		$insercao->bindParam(":uf", $uf);
		$insercao->bindParam(":cep", $cep);
		$insercao->bindParam(":compl", $complemento);

		$insercao->execute();
	}

	public function todas(){
		$consulta = $this->bd->query("SELECT pessoa.IdPessoa, pessoa.Nome, pessoa.Datanasc, pessoa.Cpf, sexo.Descricao, pessoa.Logradouro, pessoa.Numero, pessoa.Bairro, pessoa.Cidade, pessoa.CEP, pessoa.Complemento, unidadefederativa.NomeEstado, unidadefederativa.Sigla
			FROM pessoa
		INNER JOIN unidadefederativa ON pessoa.CodigoIbge = unidadefederativa.CodigoIbge
		INNER JOIN sexo ON pessoa.IdSexo = sexo.IdSexo");
		$pessoa = $consulta->fetchAll();
		return $pessoa;
	}

	public function grafico(){
		$consulta = $this->bd->query("SELECT distinct t.IdSexo,
		(SELECT count(Sigl) from sexo where Sigl='M' and IdSexo = t.IdSexo) as M,

		(SELECT count(Sigl) from sexo where Sigl='F' and IdSexo = t.IdSexo) as F

		from sexo t
		");
	}


	public function excluir($id){
		$exclusao = $this->bd->prepare("DELETE from pessoa where IdPessoa = :id");
		$exclusao->bindParam(":id", $id);
		$exclusao->execute();
	}

	public function consultarPorId($IdPessoa){
		$consulta = $this->bd->prepare("SELECT * FROM pessoa where IdPessoa = :id");
		$consulta->bindParam(":id", $IdPessoa);
		$consulta->execute();

		$pessoa = $consulta->fetch();
		return $pessoa;
	}

	public function update($IdPessoa, $nome, $datanasc, $cpf, $logadouro, $numero, $bairro, $cidade, $cep, $complemento, $uf, $sexo){
		$consulta = $this->bd->prepare("UPDATE pessoa SET Nome = :nome, Datanasc = :datanasc, Cpf = :cpf, Logradouro = :end, Numero = :nume, Bairro = :bai, Cidade = :cid, CEP = :cep, Complemento = :compl, CodigoIbge = :uf, IdSexo = :sexo where IdPessoa = :id");
		$consulta->bindParam(":id", $IdPessoa);
		$consulta->bindParam(":nome", $nome);
		$consulta->bindParam(":datanasc", $datanasc);
		$consulta->bindParam(":cpf", $cpf);
		$consulta->bindParam(":end", $logadouro);
		$consulta->bindParam(":nume", $numero);
		$consulta->bindParam(":bai", $bairro);
		$consulta->bindParam(":cid", $cidade);
		$consulta->bindParam(":cep", $cep);
		$consulta->bindParam(":compl", $complemento);
		$consulta->bindParam(":uf", $uf);
		$consulta->bindParam(":sexo", $sexo);

		$consulta->execute();
	}
}
?>