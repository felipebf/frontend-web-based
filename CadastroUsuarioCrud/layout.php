<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title><?php echo $titulo; ?></title>

    <!-- CSS  -->
    <link href="/CadastroUsuarioCrud/content/css/fonts.css" rel="stylesheet">
    <link href="/CadastroUsuarioCrud/content/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/CadastroUsuarioCrud/content/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

    <!--  Scripts-->
    <script src="/CadastroUsuarioCrud/content/js/jquery-3.2.1.min.js"></script>
    <script src="/CadastroUsuarioCrud/content/js/materialize.js"></script>
    <script src="/CadastroUsuarioCrud/content/js/init.js"></script>

</head>
<body>
    <nav class="light-blue lighten-1" role="navigation">
        <div class="nav-wrapper container">
            12/2017 Teste Frontend Web Based - Felipe Borges Ferreira
        </div>
    </nav>

<div class="section no-pad-bot" id="index-banner">
    <div class="container">

        <?php echo $conteudo; ?>

    </div>
</div>

    <footer class="page-footer orange">
        <div class="footer-copyright">
            <div class="container">
                12/2017 Teste Frontend Web Based - Felipe Borges Ferreira
            </div>
        </div>
    </footer>
    </body>
</html>