<?php

    $acao = "create";
    $id = "";
    $nome = "";
    $cpf = "";
    $sexo = "";
    $datanasc = "";
    $logadouro = "";
    $numero = "";
    $bairro = "";
    $cidade = "";
    $uf = "";
    $cep = "";
    $complemento = "";

require_once('../model/CadastrarPessoaModel.php');
if( isset($_GET["id"])){
    $acao = "update";

    $CadastrarPessoaModel = new CadastrarPessoaModel();

    $pessoa = $CadastrarPessoaModel->consultarPorId($_GET["id"]);
    $id = $pessoa["IdPessoa"];
    $nome = $pessoa["Nome"];
    $cpf = $pessoa["Cpf"];
    $sexo = $pessoa["IdSexo"];
    $datanasc = $pessoa["Datanasc"];
    $logadouro = $pessoa["Logradouro"];
    $numero = $pessoa["Numero"];
    $bairro = $pessoa["Bairro"];
    $cidade = $pessoa["Cidade"];
    $uf = $pessoa["CodigoIbge"];
    $cep = $pessoa["CEP"];
    $complemento = $pessoa["Complemento"];
}
?>
<?php
$conectar = mysqli_connect("localhost","root","","scriptbd") or die ("Erro na conexão com o Banco de Dados");
mysqli_query($conectar, 'SET NAMES utf8');

$sql = "SELECT uf.CodigoIbge, uf.Sigla, uf.NomeEstado FROM unidadefederativa as uf";

$result = mysqli_query($conectar, $sql);

?>
<?php
$conectar2 = mysqli_connect("localhost","root","","scriptbd") or die ("Erro na conexão com o Banco de Dados");
mysqli_query($conectar2, 'SET NAMES utf8');

$sql2 = "SELECT sexo.IdSexo, sexo.Sigl, sexo.Descricao FROM sexo as sexo";

$result2 = mysqli_query($conectar2, $sql2);

?>

<div class="form-horizontal">
    <form method="post" id="formCadastrarPessoa" action="../controller/CadastrarPessoaController.php?acao=<?=$acao?>" autocomplete="off">
        <div class="form-group">
            <div class="row">
                <div class="input-field col s12 l1">
                    <label>Codigo</label>
                    <input type="text" class="form-control" value="<?php echo $id ?>" name="codigo" readonly>
                </div>

                <div class="input-field col s12 l5">
                    <label>Nome</label>
                    <input type="text" class="form-control" value="<?php echo $nome ?>" name="nome" placeholder="Digite seu nome" required>
                </div>

                <div class="input-field col s12 l3">
                    <label>Cpf</label>
                    <input type="text" class="form-control" value="<?php echo $cpf ?>" name="cpf" placeholder="Digite seu cpf"" required>
                </div>

                <div class="col s12 l3">
                    <label>Sexo</label>
                    <select class="form-control" name="sexo">
                        <option>Selecione seu sexo</option>
                    <?php

                    while($row = $result2->fetch_assoc()){

                        if($row[IdSexo] == $sexo){
                            echo'
                            <option value='.$row[IdSexo].' selected>'.$row[Sigl].' - '.$row[Descricao].'</option>
                            ';
                        } else{
                        echo'
                        <option value='.$row[IdSexo].'>'.$row[Sigl].' - '.$row[Descricao].'</option>
                        ';
                        }
                    }
                    ?>
                    </select>
                </div>
            </div>
            <div class="row">
            <?php //$datanasc = implode("/", array_reverse(explode("-", $datanasc))); ?>
                <div class="input-field col s12 l2">
                    <label>Data Nascimento</label>
                    <input type="text" class="form-control" value="<?php echo $datanasc ?>" name="datanasc" placeholder="Ex: 01/01/2001" required>
                </div>

                <div class="input-field col s12 l5">
                    <label>Logadouro</label>
                    <input type="text" class="form-control" name="logadouro" value="<?php echo $logadouro ?>" placeholder="Digite o nome da Rua ou Avenida" required>
                </div>

                <div class="input-field col s12 l2">
                    <label>Numero</label>
                    <input type="text" class="form-control" name="numero" value="<?php echo $numero ?>" placeholder="Digite o número" required>
                </div>

                <div class="input-field col s12 l3">
                    <label>Bairro</label>
                    <input type="text" class="form-control" name="bairro" value="<?php echo $bairro ?>" placeholder="Digite o bairro" required>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 l3">
                    <label>Cidade</label>
                    <input type="text" class="form-control" name="cidade" value="<?php echo $cidade ?>" placeholder="Digite sua cidade" required>
                </div>

                <div class="col s12 l3">
                    <label>Unidade Federativa</label>
                    <select class="form-control" name="uf">
                        <option>Selecione seu estado</option>
                    <?php

                    while($row = $result->fetch_assoc()){

                        if($row[CodigoIbge] == $uf){
                            echo'
                            <option value='.$row[CodigoIbge].' selected>'.$row[Sigla].' - '.$row[NomeEstado].'</option>
                            ';
                        } else{
                        echo'
                        <option value='.$row[CodigoIbge].'>'.$row[Sigla].' - '.$row[NomeEstado].'</option>
                        ';
                      }
                    }
                    ?>
                    </select>
                </div>

                <div class="input-field col s12 l3">
                    <label>CEP</label>
                    <input type="text" class="form-control" name="cep" value="<?php echo $cep ?>" placeholder="Digite o CEP" required>
                </div>

                <div class="input-field col s12 l3">
                    <label>Complemento</label>
                    <input type="text" class="form-control" name="complemento" value="<?php echo $complemento ?>" placeholder="Digite o complemento">
                </div>
            </div>
        </div><br>
    <div class="form-group">
        <div class="row">
                <div class="col s6">
                    <button type="reset" class="btn btn-primary" style="float: right;">Limpar</button>
                </div>

            <div id="formSalvar">
                <div class="col s6">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        <div id="mensagem"></div>
        </div>
    </form>
    </div>
</div>
<script type="text/javascript">
$("#formCadastrarPessoa").on("submit" ,function(event) {
    event.preventDefault();

    $.ajax({
        url: $("#formCadastrarPessoa").attr("action"),
        method: $("#formCadastrarPessoa").attr("method"),
        data: $("#formCadastrarPessoa").serialize(),
        success: function(data){
            $("#mensagem").html(data);
            $("#formCadastrarPessoa").trigger('reset');
            $("#tabela").load("visualizar.php")
        }
    })
});
</script>

<style type="text/css" media="screen">
    .form-control {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        /*border: 1px solid #ccc;
        border-radius: 4px;*/
        margin-top: 3px;
        border-bottom: 1px solid #9e9e9e;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>