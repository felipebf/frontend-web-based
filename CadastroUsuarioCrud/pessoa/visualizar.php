<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/CadastroUsuarioCrud/model/CadastrarPessoaModel.php";

$CadastrarPessoaModel = new CadastrarPessoaModel();

$categorias = $CadastrarPessoaModel->todas();

$sexos = $CadastrarPessoaModel->grafico();

?>
<br><br>
<h4 class="text-center">Pessoas cadastradas</h4>
<table class="highlight responsive-table table table-bordered table-condensed tabelaPessoa text-uppercase">
    <thead class="thead-inverse">
        <tr>
            <th>Código</th>
            <th>Nome</th>
            <th>Data Nascimento</th>
            <th>Cpf</th>
            <th>Sexo</th>
            <th>Logadouro</th>
            <th>Numero</th>
            <th>Bairro</th>
            <th>Cidade</th>
            <th>CEP</th>
            <th>Complemento</th>
            <th>Editar</th>
            <th>Excluir</th>
        </tr>
    </thead>

    <tbody>

<?php foreach($categorias as $c): ?>
<?php //$datanasc = implode("/", array_reverse(explode("-", $datanasc))); ?>
    <tr>
        <td><?= $c["IdPessoa"]?></td>
        <td><?= $c["Nome"]?></td>
        <td><?= $c["Datanasc"] ?></td>
        <!--<td><?= date('d/m/Y',strtotime($c["Datanasc"])) ?></td>-->
        <td><?= $c["Cpf"]?></td>
        <td><?= $c["Descricao"]?></td>
        <td><?= $c["Logradouro"]?></td>
        <td><?= $c["Numero"]?></td>
        <td><?= $c["Bairro"]?></td>
        <td><?= $c["Cidade"]?> - <?= $c["Sigla"]?></td>
        <td><?= $c["CEP"]?></td>
        <td><?= $c["Complemento"]?></td>
        <td><a href="javascript:update(<?= $c["IdPessoa"] ?>)">Editar</a></td>
        <td><a href="javascript:excluir(<?= $c["IdPessoa"] ?>)">Excluir</a></td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>


<br><br><br>
<script>
function excluir(IdPessoa){
$.ajax({
    method: "GET",
    url: "/CadastroUsuarioCrud/controller/CadastrarPessoaController.php",
    data: { acao:"delete", id:IdPessoa },
    success: function(data){
      $("#mensagem").html(data);
      $("#tabela").load("visualizar.php");
    }
})
}

function update(IdPessoa){
$("#cadastrarPessoa").load("formulario.php?id=" + IdPessoa, function(){
    Materialize.updateTextFields(); //Apenas para o Materialize
});
}
</script>

