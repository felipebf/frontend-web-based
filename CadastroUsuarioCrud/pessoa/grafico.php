        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">

            google.load("visualization", "1", {packages: ["corechart"]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Sexo', 'Gênero'],
<?php 

$link = mysqli_connect('localhost', 'root', '', 'scriptbd');
$sth = mysqli_query($link,'SELECT IdSexo, (select sexo.descricao from sexo where pessoa.idSexo = sexo.idSexo) as sexo, COUNT(*) as quantidade FROM pessoa GROUP BY IdSexo');
while ($r = mysqli_fetch_array($sth)) {

    echo "[". "'".$r['sexo']."'". ',' . $r['quantidade'].']'.',';
}
 ?>
                ]);
                var options = {
                    title: 'Estatistica',
                    is3D: true,
                };
                var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                chart.draw(data, options);
            }
        </script>

    <div id="piechart" style="width: 900px; height: 500px;"></div>