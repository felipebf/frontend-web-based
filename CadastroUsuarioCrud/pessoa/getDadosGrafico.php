<?php

// Estrutura basica do grafico
$grafico = array(
    'dados' => array(
        'cols' => array(
            array('type' => 'string', 'label' => 'Sexo'),
            array('type' => 'number', 'label' => 'Quantidade')
        ),
        'rows' => array()
    ),
    'config' => array(
        'title' => 'Quantidade de pessoa por gênero',
        'width' => 400,
        'height' => 300
    )
);

// Consultar dados no BD
$pdo = new PDO('mysql:host=localhost;dbname=scriptbd', 'root', '');
$sql = 'SELECT IdSexo, COUNT(*) as quantidade FROM pessoa GROUP BY IdSexo';
$stmt = $pdo->query($sql);
while ($obj = $stmt->fetchObject()) {
    $grafico['dados']['rows'][] = array('v' => array(
        array('v' => $obj->IdSexo),
        array('v' => (int)$obj->quantidade)
    ));
}

// Enviar dados na forma de JSON
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($grafico);
exit(0);
?>