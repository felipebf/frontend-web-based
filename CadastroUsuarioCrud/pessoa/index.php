<?php
	ob_start();
?>
	<div id="cadastrarPessoa">
		<?php include "formulario.php"; ?>
	</div>

	<div id="tabela">
		<?php include "visualizar.php" ?>
	</div>

	<div id="grafico">
		<?php include "grafico.php" ?>
	</div>

<?php
 	$conteudo = ob_get_contents();
	$titulo = "Teste Frontend";
 	ob_end_clean ();
	include ('../layout.php');
?>